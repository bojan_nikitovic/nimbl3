//
//  AuthTokenTests.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit
import XCTest
@testable import Nimbl3

class AuthTokenTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testGetAuthTokenSuccessful() {
        let _expectation = expectation(description: "Expecting an AuthToken with \(Key.AuthToken.accessToken) string.")
        
        AuthTokenManager.getToken { (result, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(result)
            
            let authToken = result as? AuthToken
            XCTAssertNotNil(authToken)
            
            let accessToken = authToken?.access_token
            XCTAssertNotNil(accessToken)
            
            _expectation.fulfill()
        }
        
        waitForExpectations(timeout: kTimeoutIntervalForRequest) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func testGetAuthTokenFailed() {
        let _expectation = expectation(description: "Expecting an error.")
        
        let params: [String: Any] = [:]
        let body = [Key.Params.grantType: Key.AuthToken.GrantType.password,
                    Key.Params.username: "username",
                    Key.Params.password: "password"]
        
        AuthService.token(params, body: body) { (result, error) in
            XCTAssertNotNil(error)
            
            _expectation.fulfill()
        }
        
        waitForExpectations(timeout: kTimeoutIntervalForRequest) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
