//
//  SurveysTests.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit
import XCTest
@testable import Nimbl3

class SurveysTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testGetSurveys() {
        let _expectation = expectation(description: "Expecting a JSON response not nil.")
        
        SurveyService.getSurveys { (result, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(result)
            
            _expectation.fulfill()
        }
        
        waitForExpectations(timeout: kTimeoutIntervalForRequest) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
