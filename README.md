### Project Structure

The project structure contains following sections:

	* Application - Application related files (for example AppDelegate file)
	* Models
        * Entities - All entities in the application
        * Services - Contains service methods for communication with the server
	* Managers - All managers and controllers
	* View Controllers - Contains view controllers
	* UI Controls - Custom UI controls
	* Constants
	* Common
        * Extensions
        * Views - Cocoa controls subclasses
	* Resources - Resource files, storyboards etc.
	* Supporting Files - All other files related to the xcode project
	* Nimbl3Tests - Unit test classes
	* Nimbl3UITests - UI test classes
