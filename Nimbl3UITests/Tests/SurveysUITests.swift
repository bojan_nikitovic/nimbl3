//
//  Nimbl3UITests.swift
//  Nimbl3UITests
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import XCTest

class Nimbl3UITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    func testSurveyList() {
        let collectionView = app.otherElements.containing(.navigationBar, identifier:"Surveys").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element
        for _ in (0..<10) {
            collectionView.swipeLeft()
            collectionView.swipeUp()
        }
        for _ in (0..<10) {
            collectionView.swipeRight()
            collectionView.swipeDown()
        }
    }
    
    func testSurveyQuestions() {
        app.buttons["Take the Survey"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        
        let backButton = elementsQuery.buttons["Back"]
        let nextButton = elementsQuery.buttons["Next"]
        let finishButton = elementsQuery.buttons["Finish"]
        
        while !finishButton.exists {
            nextButton.tap()
        }
        
        backButton.tap()
        nextButton.tap()
        finishButton.tap()
    }
    
    func testSurveyQuit() {
        let takeSurveyButton = app.buttons["Take the Survey"]
        takeSurveyButton.tap()
        
        let dismissButton = app.navigationBars.children(matching: .button).element
        dismissButton.tap()
        
        let alertsQuery = app.alerts
        let cancelButton = alertsQuery.buttons["Cancel"]
        cancelButton.tap()
        dismissButton.tap()
        
        let quitButton = alertsQuery.buttons["Quit"]
        quitButton.tap()
        
        takeSurveyButton.tap()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
