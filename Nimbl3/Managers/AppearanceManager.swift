//
//  AppearanceManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import UIKit

class AppearanceManager: NSObject {
    // Setup appearance for the whole application.
    static func setup() {
        UINavigationBar.appearance().barTintColor = Color.NavigationBar.backgroundColor
        UINavigationBar.appearance().tintColor = Color.NavigationBar.barTintColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: Color.NavigationBar.textColor]
        
        // Fixing iPad white table view cells issue.
        UITableViewCell.appearance().backgroundColor = Color.clear
    }
}
