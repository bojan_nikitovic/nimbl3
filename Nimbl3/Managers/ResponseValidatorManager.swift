//
//  ResponseValidatorManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class ResponseValidatorManager: NSObject {
    static func validate(result: Any?, error: Error?, statusCode: Int) -> NSError? {
        var error = error
        if let resultDictionary = result as? [String: Any], error == nil {
            if let errorDomain = resultDictionary[Key.Error.error] as? String {
                let message = resultDictionary[Key.Error.description] as? String
                
                let errorDetail = [NSLocalizedDescriptionKey as NSObject: message as AnyObject] as [NSObject: AnyObject]
                error = NSError(domain: errorDomain, code: statusCode, userInfo: errorDetail as? [String : Any])
            }
        }
        
        return error as NSError?
    }
}
