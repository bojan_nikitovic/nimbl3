//
//  StorageManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import KeychainSwift

class StorageManager: NSObject {
    // Can't init
    private override init() { }
    
    static let shared = StorageManager()
    
    // Use KeychainSwift to securly store sensitive data.
    private let keychainSwift = KeychainSwift()
    
    func set(_ value: String, forKey key: String) {
        self.keychainSwift.set(value, forKey: key)
    }
    
    func get(_ key: String) -> String? {
        return self.keychainSwift.get(key)
    }
}
