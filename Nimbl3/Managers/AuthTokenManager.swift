//
//  AuthTokenManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation

class AuthTokenManager: NSObject {
    // Obtain a new auth token from the server.
    static func getToken(completion: @escaping (_ result: Any?, _ error: NSError?) -> Void) {
        guard let username = StorageManager.shared.get(Key.Params.username), let password = StorageManager.shared.get(Key.Params.password) else {
            completion(nil, nil)
            
            return
        }
        
        let params: [String: Any] = [:]
        let body = [Key.Params.grantType: Key.AuthToken.GrantType.password,
                    Key.Params.username: username,
                    Key.Params.password: password]
        
        AuthService.token(params, body: body) { (result, error) in
            if error != nil || result == nil {
                completion(result, error)
                
                return
            }
            
            guard let authToken = AuthToken(JSON: result as! [String: Any]), let accessToken = authToken.access_token else { // If the auth token is nil or access_token is nil, this is the exit path.
                
                completion(result, error)
                
                return
            }
            
            // Store access_token in local storage.
            StorageManager.shared.set(accessToken, forKey: Key.AuthToken.accessToken)
            
            completion(authToken, error)
        }
    }
}
