//
//  ServiceManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit
import Alamofire

func getHTTPMethod(method: String) -> HTTPMethod {
    switch method {
    case URI.Method.kGet:
        return HTTPMethod.get
    case URI.Method.kPost:
        return HTTPMethod.post
    default:
        return HTTPMethod.get
    }
}

// Timeout interval for network requests.
var kTimeoutIntervalForRequest: TimeInterval {
    let manager = Alamofire.SessionManager.default
    
    return manager.session.configuration.timeoutIntervalForRequest
}

class ServiceManager: NSObject {
    static func invoke(_ method: String, _ URL: String, _ params: [AnyHashable: Any]? = nil, body: [String: Any]? = nil, headers: [String: String]? = nil, completion: @escaping (_ result: Any?, _ error: NSError?) -> Void) {
        let httpMethod = getHTTPMethod(method: method)
        
        Alamofire.request(URL, method: httpMethod, parameters: body, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let statusCode = response.response?.statusCode ?? 0
            var result = response.result.value
            var error = ResponseValidatorManager.validate(result: result, error: response.result.error, statusCode: statusCode)
            
            // Token expired or unauthorized access. Try to renew the token and try again with the service call.
            if statusCode == Key.Error.Code.unauthorizedAccess && (params?[Key.Params.retry] == nil || !(params?[Key.Params.retry] as! Bool)) {
                AuthTokenManager.getToken(completion: { (tokenResult, tokenError) in
                    if tokenError != nil {
                        completion(result, error)
                        
                        return
                    }
                    
                    let authToken = tokenResult as! AuthToken
                    
                    var URL = URL
                    var params = params
                    if let expiredToken = URL.getQueryStringParameter(param: Key.AuthToken.accessToken) {
                        URL = URL.replacingOccurrences(of: expiredToken, with: authToken.access_token!)
                    }
                    
                    // Mark service call as retry in order to avoid infinite loop.
                    params?[Key.Params.retry] = true
                    
                    // Retry the service call, but this time with a new auth token.
                    ServiceManager.invoke(method, URL, params, body: body, headers: headers, completion: { (result, error) in
                        completion(result, error)
                    })
                })
                
                return
            }
            
            // In case of no internet connection.
            if let urlError = error as? URLError, urlError.code  == URLError.Code.notConnectedToInternet {
                // Return cached response.
                (result, error) = CacheManager.cached(URL) as! (Any?, NSError?)
                
                // If there is no cached response, assign error to previous value.
                if error?.code == Key.Cache.Error.Code.noCachedObject {
                    error = response.result.error as NSError?
                }
            }
            else {
                // Cache response if it's returned from server.
                CacheManager.cache(result, error, URL)
            }
            
            completion(result, error)
        }
    }
}
