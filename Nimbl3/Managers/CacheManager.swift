//
//  CacheManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

// This class is used for caching responses to UserDefaults.
class CacheManager: NSObject {
    // Cache result and error values to UserDefaults.
    static func cache(_ result: Any?, _ error: NSError?, _ key: String) {
        var object: [AnyHashable: Any] = [:]
        if result != nil {
            object[Key.Cache.result] = result
        }
        if error != nil {
            object[Key.Cache.error] = error
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        UserDefaults.standard.setValue(data, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    // Return cached result and error values from UserDefaults, 
    // if there is no cached values return kNoCachedObjectError error.
    static func cached(_ key: String) -> (Any?, Any?) {
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            let object = NSKeyedUnarchiver.unarchiveObject(with: data) as! [AnyHashable: Any]
            
            return (object[Key.Cache.result], object[Key.Cache.error])
        }
        
        // Return no cached error if there is no cached response.
        return (nil, NSError(domain: Key.Cache.domain, code: Key.Cache.Error.Code.noCachedObject, userInfo: nil))
    }
}
