//
//  ConfigurationManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import UIKit

// Initiate configuration file for the application. Check Config.plist file.
let config = NSDictionary(contentsOfFile: Bundle.main.path(forResource: Key.Config.config, ofType: "plist")!)!

class ConfigurationManager: NSObject {
    static let server = config[Key.Config.server] as! String
    
    static func setup() {
        let storageManager = StorageManager.shared
        
        // If local storage does not have access_token, use Access Token provided in Config file.
        if storageManager.get(Key.AuthToken.accessToken) == nil {
            // Use Access Token provided in Config file.
            if let accessToken = config[Key.Config.accessToken] as? String {
                storageManager.set(accessToken, forKey: Key.AuthToken.accessToken)
            }
        }
        
        // Use Username provided in Config file.
        if let username = config[Key.Config.username] as? String {
            storageManager.set(username, forKey: Key.Params.username)
        }
        
        // Use Password provided in Config file.
        if let password = config[Key.Config.password] as? String {
            storageManager.set(password, forKey: Key.Params.password)
        }
    }
}
