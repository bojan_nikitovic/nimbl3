//
//  PageViewController.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit
import Kingfisher

class PageViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl?
    
    // PageView collection data source initializer.
    let dataSource = PageViewCollectionViewDataSource()
    
    var scrollDirection: UICollectionViewScrollDirection?
    {
        return (self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection
    }
    
    var contentOffset: CGFloat {
        return self.scrollDirection == .vertical ? self.collectionView.contentOffset.y : self.collectionView.contentOffset.x
    }
    
    var collectionViewSize: CGFloat {
        return self.scrollDirection == .vertical ? self.collectionView.frame.height : self.collectionView.frame.width
    }
    
    // Return the current page based on content offset.
    var currentPage: Int {
        let currentPage = Int(round(self.contentOffset / self.collectionViewSize))
        
        return currentPage
    }
    
    var currentPageViewItem: PageViewItem {
        return self.dataSource.data[self.currentPage]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            // This is a fix for safe area bottom margin.
            self.collectionView?.contentInsetAdjustmentBehavior = .never
        }
        
        if self.scrollDirection == .vertical
        {
            // Make page control vertical.
            self.pageControl?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
        }
        
        self.collectionView?.dataSource = self.dataSource
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        (self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize = CGSize(width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView?.reloadData()
    }
    
    func setPageViewItems(_ pageViewItems: Array<PageViewItem>) {
        // If view is not yet loaded, force view load.
        if !self.isViewLoaded {
            _ = self.view
        }
        
        self.dataSource.data = Array<PageViewItem>()
        
        for pageViewItem in pageViewItems {
            if let imageURL = pageViewItem.image, let url = URL(string: imageURL) {
                let resource = ImageResource(downloadURL: url)
                
                KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            self.dataSource.data.append(pageViewItem)
        }
        
        self.collectionView?.reloadData()
        
        // Reset content offset to zero.
        self.collectionView?.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        
        self.pageControl?.numberOfPages = self.dataSource.data.count
    }
}

extension PageViewController: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.pageControl?.currentPage = self.currentPage
    }
}
