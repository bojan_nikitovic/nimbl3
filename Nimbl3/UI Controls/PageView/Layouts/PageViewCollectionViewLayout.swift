//
//  PageViewLayout.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class PageViewCollectionViewLayout: UICollectionViewFlowLayout, UICollectionViewDelegateFlowLayout {
    
    static var pageViewCollectionViewCellReuseIdentifier = String(describing: PageViewCollectionViewCell.classForCoder())
    
    // Maximum number of items per row.
    var itemsPerRow: Int! {
        return 1
    }
}
