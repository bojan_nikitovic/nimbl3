//
//  PageViewItem.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import ObjectMapper

class PageViewItem: Mappable {
    var id: String?
    var title: String?
    var description: String?
    var image: String?
    var userInfo: Any?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        title       <- map["title"]
        description <- map["description"]
        image       <- map["image"]
        userInfo    <- map["userInfo"]
    }
}
