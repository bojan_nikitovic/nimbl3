//
//  PageViewCollectionViewDataSource.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class PageViewCollectionViewDataSource: NSObject, UICollectionViewDataSource
{
    var data = Array<PageViewItem>()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let itemsPerRow = (collectionView.collectionViewLayout as! PageViewCollectionViewLayout).itemsPerRow!
        
        return (self.data.count / itemsPerRow) + ((self.data.count % itemsPerRow) != 0 ? 1 : 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let itemsPerRow = (collectionView.collectionViewLayout as! PageViewCollectionViewLayout).itemsPerRow!
        
        return min(itemsPerRow, self.data.count - section * itemsPerRow)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageViewCollectionViewLayout.pageViewCollectionViewCellReuseIdentifier, for: indexPath) as! PageViewCollectionViewCell
        
        let itemsPerRow = (collectionView.collectionViewLayout as! PageViewCollectionViewLayout).itemsPerRow!
        
        let pageViewItem = self.data[indexPath.section * itemsPerRow + indexPath.row]
        
        cell.pageViewItem = pageViewItem
        
        return cell
    }
}
