//
//  PageViewCollectionViewCell.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit
import Kingfisher

class PageViewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconImageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    
    var pageViewItem: PageViewItem? {
        didSet {
            self.prepareForReuse()
            
            if let url = URL(string: self.pageViewItem?.image ?? "") {
                self.iconImageView?.kf.setImage(with: url)
            }
            self.titleLabel?.text = (self.pageViewItem?.title ?? "")
            self.descriptionLabel?.text = (self.pageViewItem?.description ?? "")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.iconImageView?.image = nil
        self.titleLabel?.text = ""
        self.descriptionLabel?.text = ""
    }
}
