//
//  QuestionViewController.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    weak var questionnaireViewController: QuestionnaireViewController?
    
    private(set) var question: Question? {
        didSet {
            var questionText = self.question?.text ?? ""
            // Quick Fix: If question starts with \n, remove it.
            if questionText.characters.first == "\n" {
                questionText.characters.removeFirst()
            }
            self.titleLabel?.text = questionText
            
            self.descriptionLabel?.text = self.question?.help_text
            
            self.nextButton?.isSelected = QuestionnaireManager.shared.isLastQuestion(question: self.question)
        }
    }
    
    func setQuestion(question: Question?) {
        self.question = question
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController?.viewControllers.first == self {
            self.backButton.removeFromSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.questionnaireViewController?.questionnaireManager.setQuestion(question: self.question)
        
        self.questionnaireViewController?.updateProgressView(self.question)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "back" { // Back to the previous question.
            self.navigationController?.popViewController(animated: true)
            
            return false
        }
        // If it's end of the questionnaire just dismiss navigation controller.
        if QuestionnaireManager.shared.isLastQuestion(question: self.question) {
            self.dismiss(animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let questionViewController = segue.destination as? QuestionViewController { // Navigate to the next question.
            _ = questionViewController.view
            
            let question = self.questionnaireViewController?.nextQuestion(question: self.question)
            
            questionViewController.questionnaireViewController = self.questionnaireViewController
            questionViewController.question = question
        }
    }
}
