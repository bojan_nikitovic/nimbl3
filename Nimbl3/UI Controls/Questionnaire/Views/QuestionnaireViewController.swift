//
//  QuestionnaireViewController.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class QuestionnaireViewController: UIViewController {
    @IBOutlet weak var progressView: UIProgressView?
    
    var questionViewController: QuestionViewController?
    
    // Questionnaire manager variable.
    var questionnaireManager = QuestionnaireManager.shared
    
    @IBAction func dismiss(sender: AnyObject) {
        self.promptQuit()
    }
    
    // Go to the next question and update progress view.
    func nextQuestion(question: Question?) -> Question? {
        let question = QuestionnaireManager.shared.nextQuestion(question: question)
        
        self.updateProgressView(question)
        
        return question
    }
    
    // Update progress view for current question.
    func updateProgressView(_ question: Question?) {
        let questionsCount = self.questionnaireManager.questions.count
        let currentQuestionIndex = self.questionnaireManager.questions.index { (currentQuestion) -> Bool in
            return currentQuestion.id == question?.id
            } ?? 0
        
        self.progressView?.setProgress(min(1, Float(currentQuestionIndex) / Float(questionsCount - 1)), animated: true)
    }
    
    // Prompt the user before quit the questionnaire.
    func promptQuit() {
        if QuestionnaireManager.shared.isLastQuestion(question: self.questionViewController?.question) {
            self.dismiss(animated: true, completion: nil)
            
            return
        }
        
        let quitAction = UIAlertAction(title: "Quit", style: .destructive, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        })
        self.showAlertController(title: nil, message: "Are you sure you want to quit before completion?", actions: [cancelAction, quitAction])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let questionViewController = (segue.destination as? UINavigationController)?.viewControllers.first as? QuestionViewController { // Navigate to first question.
            _ = questionViewController.view
            
            self.questionViewController = questionViewController
            self.questionViewController?.questionnaireViewController = self
        }
    }
}
