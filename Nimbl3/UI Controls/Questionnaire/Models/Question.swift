//
//  Question.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import ObjectMapper

class Question: Mappable {
    var id: String?
    var text: String?
    var help_text: String?
    var answers: Array<[String: Any]>?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id              <- map["id"]
        text            <- map["text"]
        help_text       <- map["help_text"]
        answers         <- map["answers"]
    }
}
