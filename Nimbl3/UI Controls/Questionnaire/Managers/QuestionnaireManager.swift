//
//  QuestionnaireManager.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/19/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import UIKit

class QuestionnaireManager: NSObject {
    // Can't init
    private override init() { }
    
    static let shared = QuestionnaireManager()
    
    private(set) var questions: Array<Question> = []
    private(set) var currentQuestion = -1
    
    func setQuestions(questions: Array<Question>?) {
        self.questions = questions ?? []
    }
    
    // Return next question. If end of the questionnaire, nil will be returned.
    func nextQuestion(question: Question? = nil) -> Question? {
        var index = self.questions.index { (currentQuestion) -> Bool in
            return currentQuestion.id == question?.id
        }
        if index != nil {
            index = index?.advanced(by: 1)
        }
        
        self.currentQuestion = index ?? 0
        
        if self.currentQuestion > self.questions.count {
            return nil
        }
        
        return self.questions[self.currentQuestion]
    }
    
    // Set the question as current question.
    func setQuestion(question: Question? = nil) {
        let index = self.questions.index { (currentQuestion) -> Bool in
            return currentQuestion.id == question?.id
        }
        
        self.currentQuestion = index ?? 0
    }
    
    // Check if the question is last question in questionnaire.
    func isLastQuestion(question: Question?) -> Bool {
        return self.questions.last?.id == question?.id
    }
}
