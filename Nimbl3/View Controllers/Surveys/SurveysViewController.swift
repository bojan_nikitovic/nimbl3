//
//  SurveysViewController.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class SurveysViewController: PageViewController {
    @IBOutlet weak var takeSurveyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Refresh surveys when app becomes active.
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification) in
            self.refresh()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        
        let currentPage = Int(self.contentOffset) / Int(self.collectionViewSize)
        let deltaX = self.contentOffset - CGFloat(currentPage) * self.collectionViewSize
        
        self.takeSurveyButton.alpha = abs(0.5 - deltaX / self.collectionViewSize) * 2
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let surveyViewController = (segue.destination as? UINavigationController)?.viewControllers.first as? QuestionnaireViewController { // Navigate to questionnaire view controller.
            _ = surveyViewController.view
            
            let survey = Survey(JSON: ["questions": self.currentPageViewItem.userInfo ?? []])!
            
            var questions = Array<Question>()
            
            // Adopt the list of survey questions to the list of questionnaire questions.
            for surveyQuestion in survey.questions ?? []
            {
                let question = Question(JSON: [:])!
                question.id = surveyQuestion.id
                question.text = surveyQuestion.text
                question.help_text = surveyQuestion.help_text
                question.answers = surveyQuestion.answers
                
                questions.append(question)
            }
            
            surveyViewController.questionnaireManager.setQuestions(questions: questions)
            surveyViewController.questionViewController?.setQuestion(question: surveyViewController.nextQuestion(question: nil))
            surveyViewController.title = self.currentPageViewItem.title
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension SurveysViewController {
    @IBAction func actionRefresh(sender: AnyObject) {
        self.refresh()
    }
    
    // Refresh the list of surveys.
    func refresh() {
        let rightBarButtonItems = self.navigationItem.rightBarButtonItems
        
        self.navigationItem.showActivityIndicatorView(color: UIColor.white, barButtonItems: self.navigationItem.rightBarButtonItems, index: .right)
        
        SurveyService.getSurveys { (result, error) in
            self.navigationItem.hideActivityIndicatorView(barButtonItems: rightBarButtonItems, index: .right)
            
            self.reloadData(result, error)
        }
    }
    
    func reloadData(_ result: Any?, _ error: Error?) {
        if error != nil {
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                
            })
            self.showAlertController(title: nil, message: error?.localizedDescription, actions: [okAction])
            
            return
        }
        
        var pageViewItems = Array<PageViewItem>()
        
        // Adopt the list of surveys to the list of page view items.
        for survey in result as? Array<Survey> ?? [] {
            let pageViewItem = PageViewItem(JSON: [:])!
            pageViewItem.id = survey.id
            pageViewItem.title = survey.title
            pageViewItem.description = survey.description
            pageViewItem.image = survey.cover_image_large
            pageViewItem.userInfo = survey.questions?.toJSON()
            
            pageViewItems.append(pageViewItem)
        }
        
        self.setPageViewItems(pageViewItems)
    }
}
