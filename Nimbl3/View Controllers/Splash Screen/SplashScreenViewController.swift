//
//  SplashScreenViewController.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refresh()
    }
    
    // Refresh page content
    func refresh() {
        self.activityIndicatorView?.startAnimating()
        
        SurveyService.getSurveys { (result, error) in
            self.activityIndicatorView?.stopAnimating()
            
            self.reloadData(result, error)
        }
    }
    
    func reloadData(_ result: Any?, _ error: Error?) {
        if error != nil {
            let okAction = UIAlertAction(title: "Retry", style: .cancel, handler: { (action) in
                self.refresh()
            })
            self.showAlertController(title: nil, message: error?.localizedDescription, actions: [okAction])
            
            return
        }
        
        let surveysStoryboard = UIStoryboard(name: "Surveys", bundle: Bundle.main)
        
        let navigationController = surveysStoryboard.instantiateViewController(withIdentifier: "SurveysNavigationController") as? UINavigationController
        let surveysViewController = navigationController?.viewControllers.first as? SurveysViewController
        surveysViewController?.reloadData(result, error)
        
        // Make surveys navigation controller as root view controller.
        app.window?.rootViewController = navigationController
    }
}

