//
//  SurveyQuestion.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import ObjectMapper

class SurveyQuestion: Mappable {
    var id: String?
    var text: String?
    var help_text: String?
    var display_order: NSNumber?
    var short_text: String?
    var pick: String?
    var display_type: String?
    var is_mandatory: Bool?
    var correct_answer_id: String?
    var facebook_profile: String?
    var twitter_profile: String?
    var image_url: String?
    var cover_image_url: String?
    var cover_image_opacity: NSNumber?
    var cover_background_color: String?
    var is_shareable_on_facebook: Bool?
    var is_shareable_on_twitter: Bool?
    var font_face: String?
    var font_size: String?
    var tag_list: String?
    var answers: Array<[String: Any]>?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id                          <- map["id"]
        text                        <- map["text"]
        help_text                   <- map["help_text"]
        display_order               <- map["display_order"]
        short_text                  <- map["short_text"]
        pick                        <- map["pick"]
        display_type                <- map["display_type"]
        is_mandatory                <- map["is_mandatory"]
        correct_answer_id           <- map["correct_answer_id"]
        facebook_profile            <- map["facebook_profile"]
        twitter_profile             <- map["twitter_profile"]
        image_url                   <- map["image_url"]
        cover_image_url             <- map["cover_image_url"]
        cover_image_opacity         <- map["cover_image_opacity"]
        cover_background_color      <- map["cover_background_color"]
        is_shareable_on_facebook    <- map["is_shareable_on_facebook"]
        is_shareable_on_twitter     <- map["is_shareable_on_twitter"]
        font_face                   <- map["font_face"]
        font_size                   <- map["font_size"]
        tag_list                    <- map["tag_list"]
        answers                     <- map["answers"]
    }
}
