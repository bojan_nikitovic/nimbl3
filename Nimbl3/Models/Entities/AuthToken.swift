//
//  AuthToken.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import ObjectMapper

class AuthToken: Mappable {
    var access_token: String?
    var token_type: String?
    var expires_in: NSNumber?
    var created_at: NSNumber?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        access_token    <- map["access_token"]
        token_type      <- map["token_type"]
        expires_in      <- map["expires_in"]
        created_at      <- map["created_at"]
    }
}
