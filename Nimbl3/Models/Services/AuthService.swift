//
//  AuthService.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation

class AuthService: NSObject {
    // Obtain a new auth token from the server.
    static func token(_ params: [AnyHashable: Any], body: [String: Any]? = nil, completion: @escaping (_ result: Any?, _ error: NSError?) -> Void) {
        let URL = ConfigurationManager.server + URI.Routes.token
        
        ServiceManager.invoke(URI.Method.kPost, URL, params, body: body) { (result, error) in
            if error != nil || result == nil {
                completion(result, error)
                
                return
            }
            
            let accessToken = (result as! [String: Any])[Key.AuthToken.accessToken] as? String ?? ""
            
            // Store auth token in local storage.
            StorageManager.shared.set(accessToken, forKey: Key.AuthToken.accessToken)
            
            completion(result, error)
        }
    }
}
