//
//  SurveyService.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation

class SurveyService: NSObject {
    // Get the list of surveys.
    static func getSurveys(_ params: [AnyHashable: Any]? = nil, body: [String: Any]? = nil, completion: @escaping (_ result: Any?, _ error: NSError?) -> Void) {
        var access_token = params?[Key.AuthToken.accessToken] as? String
        if access_token == nil {
            // If access_token is not provided, use value from the local storage.
            access_token = StorageManager.shared.get(Key.AuthToken.accessToken)
        }
        
        // Check if access_token parameter is nil.
        assert(access_token != nil, "Invalid parameters. Params must have \(Key.AuthToken.accessToken).")
        
        let URL = ConfigurationManager.server + String(format: URI.Routes.surveys, access_token!)
        
        ServiceManager.invoke(URI.Method.kGet, URL, params, body: body) { (result, error) in
            if error != nil || result == nil {
                completion(result, error)
                
                return
            }
            
            // This is the list of all surveys returned from the server.
            let surveys = Array<Survey>.init(JSONArray: result as! [[String: Any]])
            
            // If everything succeeds, return the list of surveys.
            completion(surveys, error)
        }
    }
}
