//
//  URIConstants.swift
//  Nimbl3
//
//  Created by bojan on 10/23/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation

struct URI {
    struct Method {
        static let kGet = "GET"
        static let kPost = "POST"
    }
    
    struct Routes {
        // Endpoint for getting access token.
        static let token = "/oauth/token"
        // Endpoint for getting the list of surveys.
        static let surveys = "/surveys.json?access_token=%@"
    }
}
