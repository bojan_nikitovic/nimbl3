//
//  ColorConstants.swift
//  Nimbl3
//
//  Created by bojan on 10/23/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    static let clear = UIColor.clear
    
    struct NavigationBar {
        static let backgroundColor = UIColor(red: 18/255.0, green: 28/255.0, blue: 54/255.0, alpha: 1.0)
        static let barTintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        static let textColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    }
}
