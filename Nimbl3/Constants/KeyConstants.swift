//
//  KeyConstants.swift
//  Nimbl3
//
//  Created by bojan on 10/23/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation

struct Key {
    // Config.plist file.
    struct Config {
        static let config = "Config"
        static let accessToken = "Access Token"
        static let username = "Username"
        static let password = "Password"
        static let server = "Server"
    }
    
    struct AuthToken {
        static let accessToken = "access_token"
        
        struct GrantType {
            static let password = "password"
        }
    }
    
    struct Cache {
        static let domain = "cache"
        static let result = "result"
        static let error = "error"
        
        struct Error {
            struct Code {
                static let noCachedObject = 1010
            }
        }
    }
    
    struct Params {
        static let username = "username"
        static let password = "password"
        static let grantType = "grant_type"
        static let retry = "retry"
    }
    
    struct Error {
        static let error = "error"
        static let description = "error_description"
        
        struct Code {
            static let unauthorizedAccess = 401
        }
    }
}
