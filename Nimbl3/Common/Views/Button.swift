//
//  UIView+IBHelper.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class Button: UIButton {
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet { self.layer.shadowColor = shadowColor.cgColor }
    }
    @IBInspectable var shadowOffset: CGSize = .zero {
        didSet { self.layer.shadowOffset = shadowOffset }
    }
    @IBInspectable var shadowOpacity: Float = 0.0 {
        didSet { self.layer.shadowOpacity = shadowOpacity }
    }
    @IBInspectable var shadowRadius: CGFloat = 0.0 {
        didSet { self.layer.shadowRadius = shadowRadius }
    }
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet { self.layer.cornerRadius = cornerRadius }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet { self.layer.borderWidth = borderWidth }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet { self.layer.borderColor = borderColor.cgColor }
    }
}
