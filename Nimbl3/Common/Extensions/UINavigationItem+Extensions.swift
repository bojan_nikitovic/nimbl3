//
//  UINavigationItem+Extensions.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

enum NavigationItemIndex {
    case left
    case right
}

extension UINavigationItem {
    // Show activity indicator in UINavigationItem.
    func showActivityIndicatorView(color: UIColor? = nil, barButtonItems: [UIBarButtonItem]?, index: NavigationItemIndex) {
        self.hideActivityIndicatorView(barButtonItems: barButtonItems, index: index)
        
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityView.color = color ?? UIColor.gray
        activityView.tag = -111
        activityView.sizeToFit()
        activityView.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin,.flexibleBottomMargin]
        
        let loadingBarButtonItem = UIBarButtonItem.init(customView: activityView)
        
        switch index {
        case .left:
            self.leftBarButtonItems = [loadingBarButtonItem]
        case .right:
            self.rightBarButtonItems = [loadingBarButtonItem]
        }
        
        activityView.startAnimating()
    }
    
    func hideActivityIndicatorView(barButtonItems: [UIBarButtonItem]?, index: NavigationItemIndex) {
        switch index {
        case .left:
            self.leftBarButtonItems = barButtonItems
        case .right:
            self.rightBarButtonItems = barButtonItems
        }
    }
}
