//
//  UIViewController+Extensions.swift
//  Nimbl3
//
//  Created by Bojan Nikitovic on 10/18/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

extension UIViewController {
    // Display alert controller with title, message and actions.
    func showAlertController(title: String? = "", message: String? = "", actions: [UIAlertAction] = []) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alertController.addAction(action)
        }
        
        // IPad issue fix
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: self.view.frame.size.width
            , height: self.view.frame.size.height)
        alertController.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
